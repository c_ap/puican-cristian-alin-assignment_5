import React from "react";

class RobotForm extends React.Component{

    constructor(props){
        super(props)
        this.state={
            name : "",
            type : "",
            mass : ""
        }
        this.handleChange = (e) =>{
            this.setState({
                [e.target.name] : e.target.value
            });
        }

        this.add = () =>{
            this.props.onAdd({
                name: this.state.name,
                type: this.state.type,
                mass: this.state.mass
            });
        }
    }

    render(){
        return <div>
                <input id="name" type="text" placeholder="Name" name="name" onChange={this.handleChange}/>
                <input id="type" type="text" placeholder="type" name="type" onChange={this.handleChange}/>
                <input id="mass" type="text" placeholder="Mass" name="mass" onChange={this.handleChange}/>
                <input type="button" value="add" onClick={this.add}/>
        </div>
    }
    
}

export default RobotForm;