import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'
import RobotForm from './RobotForm'



class RobotList extends Component {
	constructor(){
		super()
		this.state = {
			robots : []
		}
		this.robotStore = new RobotStore();
		this.add = (robot) =>{
			this.robotStore.addRobot(robot);
		}
	}
	componentDidMount(){
		this.setState({
			robots : this.robotStore.getRobots()
		})
		this.robotStore.emitter.addListener('UPDATE', () => {
			this.setState({
				robots : this.robotStore.getRobots()
			})			
		})
	}
	render() {
		return (
			<div>
				 
				{
					this.state.robots.map((e, i) => 
						<Robot item={e} key={i} />
					)
				}
				<RobotForm onAdd={this.add}/>
			</div>
		)
	}
}

export default RobotList
